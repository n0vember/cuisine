= gigot de 7 heures

== Ingrédients

- 1 gigot
- thym
- laurier
- 1 oignon
- 2 gousses d'ail
- 1 echalotte
- 3 carottes
- vin blanc

== Recette

- Mettre le gigot dans son plat, le badigeonner d'huile.
- Saupoudrer de thym.
- Préparer l'ail en enlevant le germe.
- Découper aïl, oignon et echalotte et les disposer autour.
- Peler et découper en tranches les carottes puis les ajouter.
- Ajouter le vin blanc
- enfourner à 120°c pendant 7h.
