DST=public
SRC=*
ADOCS=$(wildcard $(SRC)/*.adoc)
HTMLS=$(patsubst %.adoc,%.html,$(ADOCS))
DOC_TYPE=article
DOC_FORMAT=html5

all: $(HTMLS)
	./index_generation
	asciidoctor -v -b $(DOC_FORMAT) -d $(DOC_TYPE) -o index.html -D $(DST) index.adoc

$(HTMLS): $(DST)
	asciidoctor -v -b $(DOC_FORMAT) -d $(DOC_TYPE) -o $(patsubst $(DST)/%,%,$@) -D $(DST) $(patsubst %.html,%.adoc,$@)

$(DST):
	find * -type d -exec echo mkdir -p $@/{} \;

merge:
	git checkout master
	git pull
	git merge tasting -m "Merge branch 'tasting'"
	git push
	git checkout tasting

.PHONY: all $(HTMLS)

clean:
	@rm -vrf $(DST)
	@rm -v index.adoc
