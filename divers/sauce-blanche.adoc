= sauce blanche

== Ingrédients

- 800ml d'eau
- 50g de beurre
- 50g de farine

== Recette

La sauce blanche se fait en deux temps : la création d'un roux, puis celle de la sauce.

- Faire chauffer l'eau, salée et poivrée
- Pour faire le roux, mettez, dans une casserole le beurre à fondre. Quand il est liquide (mais pas en ébullition), intégrer la farine en pluie tout en mélangeant avec un fouet. Le but de verser doucement la farine et de remuer en même temps est d'éviter la formation de grumeaux. Une fois formé, le roux forme une sorte de pâte. Sortir à ce moment la casserole du feux.
- Ajouter ensuite l'eau chaude petit à petit, sans cesser de remuer au fouet. Le principe de base est de bien mélanger au fouet au fur et à mesure pour éviter la formation de grumeaux.
 
La sauce blanche est la base de beacoup de sauces : avec du lait au lieu de l'eau, on obtient une béchamel ; avec le jus de cuisson de la viande à la place de l'eau, c'est la sauce de la blanquette... On peut également assaisonner l'eau pour varier le gout : En utilisant un bouillon, un bouquet garni, un court bouillon, ou simplement en ajoutant des épices, de l'ail...
